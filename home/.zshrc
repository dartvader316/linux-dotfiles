
. ~/.shrc

bindkey -e
autoload -Uz compinit
compinit
zstyle ':completion:*' menu select
unsetopt pathdirs


set -o promptsubst
PROMPT='%F{green}$(my_prompt)%f\$ '
