#!/bin/sh

set -eu

pdftoppm -r 300 "$1" "$2" -png
