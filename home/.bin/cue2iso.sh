#!/bin/sh

usage(){
	printf "Usage: %s [.cue file] \n" "$0"
}
[ -z "$1" ] && usage && exit 1

set -eu

CUEFILE="$1"
BINFILE="$(basename "$1" .cue).bin"

bchunk "$BINFILE" "$CUEFILE" "$BINFILE"
