#!/bin/sh

usage(){
	printf "Usage: %s [file1.epub] [file2.epub] [diff args]\n" "$0"
}

[ -z "$1" ] && usage && exit 1
[ -z "$2" ] && usage && exit 2

set -eu 

TEMP1=$(mktemp)
TEMP2=$(mktemp)

einfo -pp "$1" > "$TEMP1"
einfo -pp "$2" > "$TEMP2"

shift 
shift 

diff "$TEMP1" "$TEMP2" "$@"

rm -f "$TEMP1" "$TEMP2"
