#!/bin/sh
set -eu

for f in "$HOME/.bin/"*.sh; do
	shellcheck "$f"
done
