#!/bin/sh

set -eu

ffmpeg -i "$1" -scodec copy -vcodec copy -c:a flac -ignore_unknown -f matroska "$1.mkv"

