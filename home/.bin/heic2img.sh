#!/bin/sh

usage(){
	printf "Usage: %s [extension] [file 1] [file 2] ... [file n] \n" "$0"
}

[ -z "$1" ] && usage && exit 1

set -eu

EXT="$1"
shift

for f in "$@"; do 
	heif-convert -- "$f" "$f.$EXT"
done
