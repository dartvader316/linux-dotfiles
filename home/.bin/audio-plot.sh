#!/bin/sh

set -eu

ffmpeg -i "$1" -lavfi showspectrumpic "$2"

