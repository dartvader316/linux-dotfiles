#!/bin/sh
# example: ffmpeg -i input.mp4 -c copy -map 0 -segment_time 00:20:00 -f segment -reset_timestamps 1 output%03d.mp4

usage(){
	printf "Usage: \n"
	printf "\t # Splits into 15 minute chunks \n"
	printf "\t %s too_big_video.mkv 00:15:00 \n" "$0"
}

[ -z "$1" ] && usage && exit 1
[ -z "$2" ] && usage && exit 2

set -eu

ffmpeg -i "$1" -c copy -map 0 -segment_time "$2" -f segment -reset_timestamps 1 "%03d$1"
