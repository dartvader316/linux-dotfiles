#!/bin/sh

set -eu

for name in *; do
	# Solutions without awk break with non-english strings
	first=$(printf "%s" "$name" | awk -F "" '{print $1}')
	
	if [ -d "$name" ] && [ "$name" = "$first" ]; then
		continue
	fi
	
	destdir=$(printf "%s" "$first" | tr '[:lower:]' '[:upper:]')
	
	mkdir -p -- "$destdir"
	
	mv -i -- "$name" "$destdir"
done

