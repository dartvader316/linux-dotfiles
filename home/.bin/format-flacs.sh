#!/bin/sh

set -eu

for f in *.flac; do
	TAG_TITLE="$(metaflac --show-tag=TITLE "$f" | head -n 1 | cut -d'=' -f2 | sed "s/'\'//g" )"
	TAG_AUTHOR="$(metaflac --show-tag=ARTIST "$f" | head -n 1 | cut -d'=' -f2 | sed "s/'\'//g" )"
	
	FILENAME="$TAG_AUTHOR - $TAG_TITLE.flac"
	if [ ! -f "$FILENAME" ]
	then
		mv -- "$f" "$FILENAME";
	else
		echo "Already exists: $FILENAME. Skipping..."
	fi
done
