#!/bin/sh
usage (){
	printf "Usage: \n"
	printf "\t # Get a lossless frame at 24 second: \n"
	printf "\t %s totally_not_hentai_movie.mkv 00:00:24 nude_ikari_shinji.png \n" "$0"
}

[ -z "$1" ] && usage && exit 1
[ -z "$2" ] && usage && exit 2
[ -z "$3" ] && usage && exit 3

set -eu

ffmpeg -ss "$2" -i "$1" -frames:v 1 "$3"
