#!/bin/sh

set -eu

for f in "$@"; do
	CRC="$(crc32 "$f" | tr '[:lower:]' '[:upper:]')"
	EXT=".$(printf '%s' "$f" | tr . \\n | tail -n1)"
	BASENAME="$(basename "$f" "$EXT")"

	test ! -z "$CRC" && test ! -z "$BASENAME" && mv -- "$f" "$(printf '%s[%s]%s' "$BASENAME" "$CRC" "$EXT")"
done
