#!/bin/sh

set -eu

ddjvu -format=pdf -quality=95 -verbose "$1" "$1".pdf
