#!/bin/sh

usage(){
	printf "Usage: %s [input image] [grid size] [output image] \n" "$0"
}

[ -z "$1" ] && usage && exit 1
[ -z "$2" ] && usage && exit 2
[ -z "$3" ] && usage && exit 3

set -eu

N=$2
FILES=""

i=1; while [ $i -le $(( N*N )) ]; do  
	FILES="$FILES $1"
	i=$(( i + 1 ))  
done

montage -mode concatenate "$FILES" -tile "$N x $N" "$3"
