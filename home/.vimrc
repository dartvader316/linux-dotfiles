set noloadplugins

let mapleader = ','

set tabstop=4
set shiftwidth=4
set noexpandtab

set number
set relativenumber
set nowrap
set smartindent

set incsearch
set hlsearch

set showcmd
set ruler
set wildmenu

set list
set listchars=tab:\|\ ,trail:.,lead:.

"set foldmethod=indent
"au BufRead * normal zR

nmap gb :bnext<CR>
nmap gB :bprev<CR>

nmap <C-LEFT> :tabprevious<CR>
nmap <C-RIGHT> :tabnext<CR>
nmap <S-C-LEFT> :tabmove -1<CR>
nmap <S-C-RIGHT> :tabmove +1<Cr>

"inoremap < <><Left>

syntax on

set background=dark
colorscheme slate
hi Normal guibg=NONE ctermbg=NONE

set clipboard=unnamedplus

" Steady bar on insert
" Works on most modern terminals
set ttyfast
set ttimeout
set ttimeoutlen=100
let &t_SI = "\e[6 q"
let &t_EI = "\e[2 q"
"augroup myCmds
"au!
"autocmd VimEnter * silent !echo -ne "\e[2 q"
"augroup END


" Mostly the same as neovim does
" https://github.com/neovim/neovim/blob/master/runtime/autoload/provider/clipboard.vim
if !has('clipboard')
	function! s:cb_copy(copied)
		redraw | echom "cb_copy: " . g:cbprovider_copy
		silent call system(g:cbprovider_copy, a:copied)
	endfunction
	function! s:cb_paste()
		redraw | echom "cb_paste: " . g:cbprovider_paste
		silent let pasted = system(g:cbprovider_paste)
		return substitute(pasted, '[^[:print:]\r\n\t]', '', 'g')
	endfunction
	
	if !empty($WAYLAND_DISPLAY) && executable('wl-copy')
		let g:cbprovider_paste = 'wl-paste --no-newline'
		let g:cbprovider_copy = 'wl-copy'
	elseif !empty($DISPLAY) && executable('xsel')
		let g:cbprovider_paste = 'xsel -ob'
		let g:cbprovider_copy = 'xsel -ib'
	endif
	
	if exists('g:cbprovider_copy') && exists('g:cbprovider_paste')
		" FIXME need to add support without unnamedplus behavior
		if exists('##TextYankPost')
			augroup cb_yank
				autocmd!
				autocmd TextYankPost * exec 'call s:cb_copy(@")'
			augroup END
		else
			xnoremap <silent> y y:call <SID>cb_copy(@")<CR>
		endif
		
		nnoremap <silent> p :let @"=<SID>cb_paste()<CR>p
		nnoremap <silent> P :let @"=<SID>cb_paste()<CR>P
	endif
endif

filetype plugin on
filetype plugin indent on


" Linting
" ===================
set signcolumn=yes

let g:errorsigns_errortext = "E"
let g:errorsigns_warningtext = "W"

execute "sign define errorsigns_error"
			\ " text=" . g:errorsigns_errortext .
			\ " linehl=ErrorMsg"
			\ " texthl=ErrorMsg"

execute "sign define errorsigns_warning" 
			\ " text=" . g:errorsigns_warningtext .
			\ " linehl=Todo"
			\ " texthl=Todo"

let s:errorsigns_dict = {
			\ 'e': 'errorsigns_error', 
			\ 'w': 'errorsigns_warning', 
			\ '': 'errorsigns_error',
			\ }

let s:positions = {}

function! s:ShowErrorAtCursor()
	let [l:bufnr, l:lnum] = getpos(".")[0:1]
	let l:bufnr = bufnr("%")
	for l:d in getqflist()
		if (l:d.bufnr != l:bufnr || l:d.lnum != l:lnum)
			continue
		endif
		redraw | echomsg l:d.text
	endfor
	echo
endfunction

function! s:RemoveErrorMarkers()
	for l:key in keys(s:positions)
		execute ":sign unplace " . l:key
	endfor

	let s:positions = {}

endfunction

function! s:SetErrorMarkers()
	
	call s:RemoveErrorMarkers()

	for l:d in getqflist()
		if (l:d.bufnr == 0 || l:d.lnum == 0)
			continue
		endif

		let l:key = l:d.bufnr . l:d.lnum
		if has_key(s:positions, l:key)
			continue
		endif
		let s:positions[l:key] = 1
		
		"echom l:d.type
		let l:type_name = tolower(l:d.type)
		if has_key(s:errorsigns_dict, l:type_name)
			let l:name = s:errorsigns_dict[l:type_name]
			execute ":sign place " . l:key . " line=" . l:d.lnum . " name=" .
					\ l:name . " buffer=" . l:d.bufnr
		endif
	endfor

endfunction

function! s:DoLint(make_single)
	if a:make_single
		silent make! %
	else
		silent make!
	endif

	call <SID>SetErrorMarkers()
	silent redraw!
endfunction

command CompilerErrorAtCursor call s:ShowErrorAtCursor()

augroup Linting
	autocmd!
	au FileType c compiler gcc
	au FileType sh compiler shellcheck
	au FileType python compiler pylint
	au FileType rust compiler rustc
	au FileType go compiler go
	au FileType cpp compiler gcc
	au FileType xhtml compiler xmllint
	au FileType javascript compiler eslint

	"au FileType python,sh,xhtml,javascript au BufWritePost <buffer> call <SID>DoLint(v:true)
	"au FileType go,c,cpp au BufWritePost <buffer> call <SID>DoLint(v:false)
augroup END


" Completion
" =========================================
set completeopt=menuone,longest,noinsert
set omnifunc=syntaxcomplete#Complete
set pumheight=5
set shortmess+=c

augroup Completion
	autocmd!

	au FileType html inoremap < <<C-x><C-o>
	au FileType html inoremap / /<C-x><C-o>

	au FileType c,js inoremap . .<C-x><C-o>
augroup END


" Colortheme
" ======================

function! SynStack ()
	for i1 in synstack(line("."), col("."))
		let i2 = synIDtrans(i1)
		let n1 = synIDattr(i1, "name")
		let n2 = synIDattr(i2, "name")
		echo n1 "->" n2
	endfor
endfunction

noremap hh :call SynStack()<CR>

if &term =~ "256"
	if $COLORTERM == "truecolor" || $COLORTERM == "24bit"
		set termguicolors
	endif
endif

" mkdir -p ~/.vim/pack/themes/opt
" cd ~/.vim/pack/themes/opt
" git clone https://github.com/tomasiser/vim-code-dark
if has('packages')
	if isdirectory(expand("~/.vim/pack/themes/opt/vim-code-dark/"))
		let g:codedark_conservative=0
		let g:codedark_modern=0
		let g:codedark_italics=0
	
		packadd vim-code-dark
		colorscheme codedark
	endif
endif
