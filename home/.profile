. /etc/profile

export GOPATH=$HOME/.go

export PATH="$PATH:$HOME/.local/bin:$HOME/.bin"


[ -f "$HOME/.cargo/env" ] && . "$HOME/.cargo/env" # rustup
[ -f "$HOME/.ghcup/env" ] && . "$HOME/.ghcup/env" # ghcup

[ -d "$GOPATH/bin" ] && export PATH="$PATH:$GOPATH/bin" # go
[ -d "$HOME/.npm-packages/bin" ] && export PATH="$PATH:$HOME/.npm-packages/bin" # npm

