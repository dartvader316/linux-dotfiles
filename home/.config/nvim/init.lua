------------------------------
-- Build-in neovim settings
------------------------------

local tab_size = 4
vim.o.tabstop = tab_size
vim.o.shiftwidth = tab_size
vim.o.expandtab = false

vim.o.number = true
vim.o.relativenumber = true
vim.o.wrap = false
vim.o.smartindent = true
vim.o.clipboard = "unnamedplus"
vim.o.syntax = "on"

vim.opt.list = true
vim.opt.listchars = {tab = "| ", trail = ".", lead = ".", eol = "¬"}

vim.g.netrw_liststyle = 3

vim.g.mapleader = ","

vim.keymap.set("n", "ts", "<cmd>tab split<cr>")
vim.keymap.set("n", "gb", "<cmd>bnext<cr>")
vim.keymap.set("n", "gB", "<cmd>bprev<cr>")

vim.keymap.set("n", "<C-LEFT>", "<cmd>tabprevious<cr>")
vim.keymap.set("n", "<C-RIGHT>", "<cmd>tabnext<cr>")
vim.keymap.set("n", "<S-C-LEFT>", "<cmd>tabmove -1<cr>")
vim.keymap.set("n", "<S-C-RIGHT>", "<cmd>tabmove +1<cr>")

vim.o.foldmethod = "indent"
vim.api.nvim_create_autocmd("BufRead", {
	callback = function()
		vim.cmd("normal zR")
	end,
})

vim.api.nvim_create_autocmd({"BufEnter", "FocusGained", "InsertLeave"}, {
	callback = function()
		vim.wo.relativenumber = true
	end,
})
vim.api.nvim_create_autocmd({"BufLeave", "FocusLost", "InsertEnter"}, {
	callback = function()
		vim.wo.relativenumber = false
	end,
})

-- FIXME disables treesitter startup, can break at any moment
-- Neovim 0.10.0+ tries to use treesitter parsers for some languages
-- like lua by default, but this breaks if you try to run it without
-- any treesitter parsers installed
if vim.fn.has('nvim-0.10') == 1 then
	vim.treesitter.start = function() return end
end

-- Notify about missing components
local notify_missing = false

----------------------------
-- Packages
----------------------------
-- `git clone --depth=1 https://github.com/savq/paq-nvim.git "$HOME/.local/share/nvim/site/pack/paqs/start/paq-nvim"`
if not pcall(require, "paq") then
	if notify_missing then
		vim.notify_once("no paq.")
	end
	return
end

require "paq" {
	"savq/paq-nvim", -- Let Paq manage itself
	"tomasiser/vim-code-dark",
	-- LSP
	{"neovim/nvim-lspconfig", opt = true},
	{"hrsh7th/nvim-cmp", opt = true},
	{"hrsh7th/cmp-nvim-lsp", opt = true},
	{"L3MON4D3/LuaSnip", opt = true},
	{"saadparwaiz1/cmp_luasnip", opt = true},
	-- DAP
	{"mfussenegger/nvim-dap", opt = true},
	{"mfussenegger/nvim-dap-python", opt = true},
}

-----------
-- Theme
-----------
if vim.fn.has('nvim-0.10') == 1 then
	-- Restore default vim colors to not break themes which depend on it
	vim.cmd("colorscheme vim")
end


if vim.o.term:match("256") then
	if vim.fn.getenv("COLORTERM") == "truecolor" or vim.fn.getenv("COLORTERM") == "24bit" then
		vim.o.termguicolors = true
	end
end
vim.g.codedark_conservative=0
vim.cmd("colorscheme codedark")

--Fixes semantic hightlighting for nvim 0.9+
--https://gist.github.com/swarn/fb37d9eefe1bc616c2a7e476c0bc0316
if vim.fn.has('nvim-0.9') == 1 then
	local links = {
		['@lsp.type.class'] = 'ClassDecl',
		['@lsp.type.struct'] = 'StructDecl',
		['@lsp.type.enum'] = 'EnumDecl',
		['@lsp.type.namespace'] = 'Namespace',
	}
	
	for newgroup, oldgroup in pairs(links) do
		vim.api.nvim_set_hl(0, newgroup, { link = oldgroup, default = true })
	end
end

------------------
-- Tab line
-----------------
function tabline_titles(tabs_n)
	
	local files = {}
	local names = {}
	local counts = {}

	for i = 1, tabs_n, 1 do
		local buflist = vim.fn.tabpagebuflist(i)
		local winnr = vim.fn.tabpagewinnr(i)
		local bufnr = buflist[winnr]
		local file = vim.fn.bufname(bufnr)

		local name = vim.fn.pathshorten(vim.fn.fnamemodify(file, ':p:~:t'))
		table.insert(files, file)
		table.insert(names, name)

		counts[name] = (counts[name] or 0) + 1;

	end
	
	local ret = {}
	local linecount = 0

	local max_width = math.floor(vim.o.columns * 0.8)

	for i = 1, #files, 1 do
		local file = files[i]
		local name = names[i]
		if file == '' then
			table.insert(ret, '[No name]')
		else
			if counts[name] > 1 then
				local path_name = vim.fn.pathshorten(vim.fn.fnamemodify(file, ':p:.'))
				table.insert(ret, path_name)
			else
				table.insert(ret, name)
			end
		end

		linecount = linecount + string.len(ret[#ret])

		if linecount > max_width then
			break
		end
	end

	return ret
end

function tabline_cell(index, title)

	local is_selected = vim.fn.tabpagenr() == index
	local buflist = vim.fn.tabpagebuflist(index)
	local winnr = vim.fn.tabpagewinnr(index)
	local bufnr = buflist[winnr]
	local hl = (is_selected and '%#TabLineSel#' or '%#TabLine#')
	local hl_sep = (is_selected and '%#Include#' or '%#TabLine#') -- separator coloring
	local modified = vim.fn.getbufvar(bufnr, '&modified') == 1 and '[+] ' or ''
	
	return 
		hl_sep .. '▎'
		.. hl 
		.. '%' .. index .. 'T'
		.. title .. ' '
		.. modified
		.. '%#TabLineFill#%T'
end

function my_tabline()
	local total_width = vim.o.columns
	local line = ''
	local tabs_n = vim.fn.tabpagenr('$')
	local titles = tabline_titles(tabs_n)

	for i = 1, #titles, 1 do
		line = line .. tabline_cell(i, titles[i])
	end
	line = line .. '%#TabLineFill#%T'

	if tabs_n ~= #titles then
		line = line .. "..."
	end

	if tabs_n > 1 then
		line = line .. '%=%#TabLine#' .. tabs_n
	end

	return line
end

vim.opt.tabline = "%!v:lua.my_tabline()"

------------------
-- Status line
------------------

local is_git = false
if vim.fn.executable("git") == 1 then
	local git_is_git = vim.fn.trim(vim.fn.system("git rev-parse --is-inside-work-tree",true))
	
	is_git = git_is_git == "true"
end
function statusline_git() 
	if is_git == true then
		local branch = vim.fn.trim(vim.fn.system("git rev-parse --abbrev-ref HEAD",true))

		return branch
	end

	return ""
end

local my_statusline = {
	"%{%v:lua.statusline_git()%}",
	"%f",
	"%h%w%m%r%=%-14.(%l,%c%V%)",
	"%P"
}

vim.o.statusline = table.concat(my_statusline, ' ')

------------------
-- Complex setup
------------------

if vim.fn.has('nvim-0.8') ~= 1 then
	if notify_missing then
		vim.notify_once("nvim version < 8.")
	end

	return
end

vim.cmd("packadd nvim-lspconfig")
vim.cmd("packadd nvim-cmp")
vim.cmd("packadd cmp-nvim-lsp")
vim.cmd("packadd LuaSnip")
vim.cmd("packadd cmp_luasnip")
vim.cmd("packadd nvim-dap")
vim.cmd("packadd nvim-dap-python")


---------------------
-- LSPconfig setup
---------------------
vim.opt.completeopt = {"menu", "menuone", "noselect"}

local log = require("vim.lsp.log")

local lspconfig = require("lspconfig")
local cmp = require("cmp");
local cmp_types = require('cmp.types')
local capabilities = require("cmp_nvim_lsp").default_capabilities()
local luasnip = require("luasnip")

local select_opts = {behavior = cmp.SelectBehavior.Select}
local cmp_mapping = {
	["<Up>"] = cmp.mapping.select_prev_item(select_opts),
	["<Down>"] = cmp.mapping.select_next_item(select_opts),
	["<C-Space>"] = cmp.mapping.complete(),
	["<CR>"] = cmp.mapping.confirm({ select = true }),
	['<Tab>'] = cmp.mapping(function(fallback)
		local col = vim.fn.col('.') - 1

		if cmp.visible() then
			cmp.select_next_item(select_opts)
		elseif col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') then
			fallback()
		else
			cmp.complete()
		end
	end, {'i', 's'}),
}

vim.api.nvim_create_autocmd("LspAttach", {
	callback = function(ev)
		local opts = { buffer = ev.buf }
		vim.keymap.set("n", "gD",vim.lsp.buf.declaration, opts)
		vim.keymap.set("n", "gd",vim.lsp.buf.definition, opts)
		vim.keymap.set("n", "gim",vim.lsp.buf.implementation, opts)
		vim.keymap.set("n", "<leader>ca",vim.lsp.buf.code_action, opts)
		vim.keymap.set("n", "<leader>ss",vim.lsp.buf.signature_help, opts)
		vim.keymap.set("n", "<leader>sh",vim.lsp.buf.hover, opts)
	end,
})


cmp.setup {
	sources = {
		{ name = "nvim_lsp", priority = 1},
		-- { name = "luasnip", priority = 2},
	},
	preselect = cmp.PreselectMode.None,
	window = {
		--completion = cmp.config.window.bordered(),
		--documentation = cmp.config.window.bordered(),
	},
	mapping = cmp.mapping.preset.insert(cmp_mapping),
	snippet = {
		expand = function(args)
			luasnip.lsp_expand(args.body)
		end
	},
	sorting = {
		priority_weight = 1.0,
		comparators = {
			cmp.config.compare.offset,
			cmp.config.compare.exact,
			cmp.config.compare.sort_text,
		},
	},
	performance = {
		max_view_entries = 30,
	}
}

lspconfig.clangd.setup {
	capabilities = capabilities,
}

-- `rustup component add rust-analyzer`
lspconfig.rust_analyzer.setup {
	capabilities = capabilities,
}
 
lspconfig.gopls.setup {
	settings = {
		gopls = {
			semanticTokens = true
		},
	},
	capabilities = capabilities,
}

lspconfig.hls.setup {
	settings = {
		haskell = {
			plugin = {
				semanticTokens = {
					globalOn = true
				},
			},
		},
	},
	capabilities = capabilities,
}

-- https://github.com/python-lsp/python-lsp-server?tab=readme-ov-file#windows-and-linux-installation
-- install python-lsp-server, pyflakes, mypy
lspconfig.pylsp.setup {
	settings = {
		pylsp = {
			plugins = {
				-- no formatting
				black = { enabled = false },
				autopep8 = { enabled = false },
				yapf = { enabled = false },
				-- linters
				pylint = { enabled = false },
				pyflakes = { 
					enabled = true,
				},
				pycodestyle = { enabled = false },
				-- type cheking
				pylsp_mypy = { 
					enabled = true,
					live_mode = true,
					strict = false,
					report_progress = true,
				},
				-- import sorting
				pyls_isort = { enabled = false },
			},
		}
	},
	capabilities = capabilities,
}

lspconfig.ts_ls.setup {
	capabilities = capabilities,
}

lspconfig.jdtls.setup {
	capabilities = capabilities,
}

lspconfig.fortls.setup {
	capabilities = capabilities,
}

---------------
-- Debugging
---------------
local dap = require("dap")
require("dap-python").setup("python3")

vim.keymap.set("n", "<leader>cd", function() dap.continue() end)
vim.keymap.set("n", "<leader>tr", function() dap.repl.toggle() end)
vim.keymap.set("n", "<leader>tb", function() dap.toggle_breakpoint() end)
vim.keymap.set("n", "<leader>sb", function() dap.set_breakpoint() end)
vim.keymap.set("n", "<leader>re", function() dap.repl.execute(vim.fn.input("DAP command: ")) end, { noremap = true })


dap.adapters.gdb = {
	type = "executable",
	command = "gdb",
	args = { "--quiet", "--interpreter=dap" },
}


-- because vim.fs.filereadable does not work
function file_exists(name)
	local f = io.open(name, "r")
	return f ~= nil and io.close(f)
end


local lldb_path = "/usr/bin/lldb-vscode"

-- on Debian it is /usr/bin/lldb-vscode-${LLVM_VERSION}
if not file_exists(lldb_path) then
	local lldbs = vim.split(vim.fn.glob("/usr/bin/lldb-vscode-*"), "\n", {trimempty=true})

	if #lldbs > 0 then
		lldb_path = lldbs[1]
	end
end

dap.adapters.lldb = {
	type = "executable",
	command = lldb_path, -- must be absolute path
	name = "lldb"
}

local last_exe = ""
local last_args = ""

function get_debug_exe()
	local input_exe = vim.fn.input("Executable: ", last_exe)
	
	local ret = ""
	if input_exe[1] == '/' then
		ret = input_exe
	else
		ret = vim.fn.getcwd() .. "/" .. input_exe
	end

	if file_exists(ret) then
		last_exe = input_exe
	end

	return ret
end

function get_debug_args()
	local input_args = vim.fn.input("Args: ", last_args)
	last_args = input_args
	return vim.split(input_args, " ")
end

dap.configurations.c = {
	{
		name = "Launch in gdb",
		type = "gdb",
		request = "launch",
		args = function()
			return get_args()
		end,
		program = function()
			return get_exe()
		end,
		cwd = "${workspaceFolder}",
		stopAtBeginningOfMainSubprogram = false,
	},
	{
		name = 'Launch in lldb',
		type = 'lldb',
		request = 'launch',
		args = function()
			return get_debug_args()
		end,
		program = function()
			return get_debug_exe()
		end,
		cwd = '${workspaceFolder}',
		stopOnEntry = false,
		runInTerminal = false,
	},
}

dap.configurations.cpp = dap.configurations.c


function get_debug_rust_project() 
	if vim.fn.executable("cargo") == 1 then
		local manifest = vim.fn.system("cargo read-manifest",true)
		if string.len(manifest) > 0 then
			local manifest_tbl = vim.json.decode(manifest)
			return "target/debug/" .. manifest_tbl["name"]
		end
	end
	return get_debug_exe()
end
dap.configurations.rust = {
	{
		name = "Debug Rust project in GDB",
		type = "gdb",
		request = "launch",
		program = get_debug_rust_project,
		cwd = "${workspaceFolder}",
		stopAtBeginningOfMainSubprogram = false,
	},
	{
		name = "Debug Rust project in LLDB",
		type = 'lldb',
		request = 'launch',
		program = get_debug_rust_project,
		cwd = '${workspaceFolder}',
		stopOnEntry = false,
		args = {},
		-- runInTerminal = false,
	},
}

dap.adapters.delve = {
	type = 'server',
	port = '${port}',
	executable = {
	command = 'dlv',
	args = {'dap', '-l', '127.0.0.1:${port}'},
	-- add this if on windows, otherwise server won't open successfully
	-- detached = false
  }
}

dap.configurations.go = {
	{
		type = "delve",
		name = "Debug",
		request = "launch",
		program = "${file}",
		--console = "internalTerminal",
		args = {},
	},
	{
		type = "delve",
		name = "Debug test", -- configuration for debugging test files
		request = "launch",
		mode = "test",
		program = "${file}",
		args = {},
	},
	{
		type = "delve",
		name = "Debug test (go.mod)",
		request = "launch",
		mode = "test",
		program = "./${relativeFileDirname}",
		args = {},
	}
}
-----------------
-- Debugger UI
-----------------

local widgets = require("dap.ui.widgets")

-- If it works, don't touch it.
local sidebar_scopes = widgets.sidebar(
	widgets.scopes, 
	{ width=30 },
	"vsplit")
local sidebar_frames = widgets.sidebar(
	widgets.frames, 
	{ width=30, height=10 },
	"belowright split")

local debug_ui = false
local my_ui_open = function()
	if not debug_ui then
		
		sidebar_scopes.toggle()
		vim.cmd("wincmd h");
		sidebar_frames.toggle()
		vim.cmd("wincmd l");
		dap.repl.toggle({}, "belowright split")

		debug_ui = true
	end
end

local my_ui_close = function()
	if debug_ui then
		
		dap.repl.toggle()
		sidebar_scopes.toggle()
		sidebar_frames.toggle()
		debug_ui = false
	end
end

dap.listeners.before["launch"]["my-ui"] = function(session, body)
	my_ui_open()
end
dap.listeners.before["event_terminated"]["my-ui"] = function(session, body)
	my_ui_close()
end

dap.listeners.before["attach"]["my-ui"] = dap.listeners.before["launch"]["my-ui"]
dap.listeners.before["event_exited"]["my-ui"] = dap.listeners.before["event_terminated"]["my-ui"]


------------------
-- init.lua end
------------------
