.POSIX:

TAR_ETC = myetc.tar
TAR_HOME = myhome.tar

FILES = $(TAR_ETC) $(TAR_HOME)

all: $(FILES)

$(TAR_ETC): etc
	tar cvf $@ $?
$(TAR_HOME): home
	tar cvf $@ -C $? .

install-etc: $(TAR_ETC)
	tar -xvf $< -C /
install-home: $(TAR_ETC)
	tar -xvf $< -C $(HOME)

clean:
	rm -f $(FILES)
